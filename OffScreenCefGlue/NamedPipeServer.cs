using System.IO;
using System.IO.Pipes;
using OffScreenCefGlue.Common;
using ProtoBuf;

namespace OffScreenCefGlue
{
    public class NamedPipeServer : BrowserGateway
    {
        public NamedPipeServer(Browser browser): base(browser)
        {            
        }

        public override void Run()
        {
            using(NamedPipeServerStream pipe = new NamedPipeServerStream("OffScreenCefGlue.Pipe", PipeDirection.InOut))
            {
                bool shouldContinue = true;
                while(shouldContinue)
                {
                    if(!pipe.IsConnected)
                    {
                        pipe.WaitForConnection();
                    }                        

                    OffScreenCefGlueBrowserCommand command = Serializer.Deserialize<OffScreenCefGlueBrowserCommand>(pipe);
                    switch(command.CommandName)
                    {
                        case "kill":
                        {
                            shouldContinue = false;
                            break;
                        }
                        default:
                        {                            
                            break;
                        }
                    }
                }
            }
        }
    }
}