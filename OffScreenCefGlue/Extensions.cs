using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace OffScreenCefGlue
{
    public static class Extensions
    {
        public static void WaitForCompletion(this Task task)
        {
            while(!task.IsCompleted)
            {
                Console.Write(".");
                Thread.Sleep(100);
            }
        }

        public static void SendMessage(this NetworkStream stream, string message)
        {
            byte[] messageBytes = Encoding.ASCII.GetBytes(message);
            stream.Write(messageBytes, 0, messageBytes.Length);
        }

        public static string ReadMessageFromStream(this NetworkStream stream)
        {
            byte[] messageBytes = new byte[1024];
            int position = 0;
            StringBuilder messageBuilder = new StringBuilder();
            
            while (!stream.DataAvailable)
            {
                //Wait for data to come through
                Thread.Sleep(100);
            }

            //While there is data read it into the string builder
            while (stream.DataAvailable)
            {                
                int amountRead = stream.Read(messageBytes, position, messageBytes.Length);                
                char[] messageChars = Encoding.ASCII.GetString(messageBytes).ToCharArray();
                messageBuilder.Append(messageChars, 0, amountRead);                
            }
            return messageBuilder.ToString();
        }

        public static byte[] ReadBytesFromStream(this NetworkStream stream)
        {
            byte[] messageBytes = new byte[1024];
            int position = 0;
            while (!stream.DataAvailable)
            {
                //Wait for data to come through
                Thread.Sleep(100);
            }
            List<byte> bytes = new List<byte>();
            //While there is data read it into the string builder
            while (stream.DataAvailable)
            {
                int amountRead = stream.Read(messageBytes, position, 1024);
                position += amountRead;                
                for(int i = 0; i < amountRead; i++)
                    bytes.Add(messageBytes[i]);
            }
            return bytes.ToArray();
        }
    }
}