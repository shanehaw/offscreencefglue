using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OffScreenCefGlue
{
    public class NavigationTask
    {
        private Queue<Task<int>> navigationTasks;
        private Task jsContextCreated;
        public NavigationTask(Queue<Task<int>> navigationTasks, Task jsContextCreated)
        {
            this.navigationTasks = navigationTasks;
            this.jsContextCreated = jsContextCreated;
        }

        public bool IsCompleted()
        {            
            bool completed = true;                         
            foreach(Task<int> navTask in navigationTasks)
            {
                if(!navTask.IsCompleted)
                {
                    completed = false;
                    break;
                }
            }

            if(completed && !jsContextCreated.IsCompleted)
            {
                completed = false;
            }
            return completed;
        }

        public (Exception, List<int>) NavigationResult()
        {
            Exception ex = null;
            List<int> httpStatusCodes = new List<int>();
            foreach(Task<int> navTask in navigationTasks)
            {
                if(navTask.IsFaulted)
                {
                    ex = navTask.Exception.GetBaseException();
                } 
                else
                {
                    httpStatusCodes.Add(navTask.Result); 
                } 
            }

            if(ex == null && jsContextCreated.IsFaulted)
            {
                ex = jsContextCreated.Exception.GetBaseException();
            }
            return (ex, httpStatusCodes);
        }        
    }
}