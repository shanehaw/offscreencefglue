using System;
using Xilium.CefGlue;

namespace OffScreenCefGlue.CefGlue
{
    internal interface CefBrowserAccepter
    {
        void Accept(CefBrowser browser);
    }
}

