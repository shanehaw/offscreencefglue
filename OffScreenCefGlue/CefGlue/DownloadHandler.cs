using System;
using System.Collections.Generic;
using System.IO;
using Xilium.CefGlue;

//CefDownloadHandler

namespace OffScreenCefGlue.CefGlue
{
    public class DownloadHandler: CefDownloadHandler
    {
        public byte[] LastDownloadedItem { private set; get; }

        protected override void OnBeforeDownload(CefBrowser browser, CefDownloadItem downloadItem, string suggestedName, CefBeforeDownloadCallback callback)
        {
            Logger.Instance.Write($"Starting download from {downloadItem.OriginalUrl} to file {downloadItem.FullPath}");
            base.OnBeforeDownload(browser, downloadItem, suggestedName, callback);
        }

        protected override void OnDownloadUpdated(CefBrowser browser, CefDownloadItem downloadItem, CefDownloadItemCallback callback)
        {
            if(downloadItem.IsComplete && downloadItem.IsValid)
            {
                Logger.Instance.Write($"Successfully finished downloading {downloadItem.OriginalUrl} to file {downloadItem.FullPath}");
                LastDownloadedItem = File.ReadAllBytes(downloadItem.FullPath);
            }
            base.OnDownloadUpdated(browser, downloadItem, callback);
        }        
    }
}