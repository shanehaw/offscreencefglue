using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xilium.CefGlue;

namespace OffScreenCefGlue.CefGlue
{
    internal class LoadHandler : CefLoadHandler, CefBrowserAccepter
    {
        private class NavigationResult
        {
            public int NavigationHttpStatusCode { get; private set; }
            public ApplicationException Exception { get; private set; }

            public NavigationResult(int navigationHttpStatusCode)
            {
                this.Exception = null;
                this.NavigationHttpStatusCode = navigationHttpStatusCode;
            }

            public NavigationResult(ApplicationException exception)
            {
                this.Exception = exception;
                this.NavigationHttpStatusCode = -1;
            }

            public override string ToString()
            {
                return $"{(Exception == null ? "null" : "Exception")}; {NavigationHttpStatusCode}";
            }
        }

        internal bool InitialNavigationFinished { get; private set; }        
        private Queue<NavigationResult> navigationResults;
        private Queue<TaskCompletionSource<int>> navigationLoadingTasks;
        private Queue<Task<int>> navigationTasks;  
        private CefBrowser browser;

        private LoadHandler()
        {            
            InitialNavigationFinished = false;            
            navigationLoadingTasks = new Queue<TaskCompletionSource<int>>();
            navigationResults = new Queue<NavigationResult>();    
            navigationTasks = new Queue<Task<int>>();        
        }

        public static (LoadHandler, Queue<Task<int>>)  Initialize()
        {
            LoadHandler loadHandler = new LoadHandler();
            return (new LoadHandler(), loadHandler.navigationTasks);
        }

        public void Accept(CefBrowser browser)
        {
            if(this.browser == null)
            {
                this.browser = browser;
            }            
        }

        public Queue<Task<int>> Navigate(string url)
        {            
            if (browser == null)
            {
                throw new ApplicationException("System not ready to navigate yet. Please wait for system to be finished loading.");                
            }
            Logger.Instance.Write($"Navigate to {url}");
            navigationTasks.Clear();
            navigationResults.Clear();

            Console.WriteLine($"**** currentNavigationResults = {string.Join(",", navigationResults.Select(r => r.ToString()))}");
            Console.WriteLine($"**** browser.FrameCount = {browser.FrameCount}");
            Console.WriteLine($"**** browser.GetFrameNames() = {string.Join(",", browser.GetFrameNames())}");
            Console.WriteLine($"**** browser.GetFrameIdentifiers() = {string.Join(",", browser.GetFrameIdentifiers().Select(i => i.ToString()))}");
            Console.WriteLine($"**** browser.GetMainFrame() = {browser.GetMainFrame().Name}");
            Console.WriteLine($"**** browser.GetFocusedFrame() = {browser.GetFocusedFrame().Name}");


            CefFrame frame = browser.GetMainFrame();            
            frame.LoadUrl(url);
            return navigationTasks;
        }

        protected override void OnLoadStart(CefBrowser browser, CefFrame frame, CefTransitionType transitionType)
        {
            if (frame.IsMain)
            {
                Logger.Instance.Write($"START: {frame.Url}");                             
            }
        }

        protected override void OnLoadEnd(CefBrowser browser, CefFrame frame, int httpStatusCode)
        {
            if (frame.IsMain)
            {
                Logger.Instance.Write($"END: {frame.Url}, {httpStatusCode}");
                navigationResults.Enqueue(new NavigationResult(httpStatusCode));
            }
        }

        protected override void OnLoadingStateChange(CefBrowser browser, bool isLoading, bool canGoBack, bool canGoForward)
        {
            bool isFinishedLoading = !isLoading;
            if(isFinishedLoading)
            {                
                Logger.Instance.Write($"Finished Loading");
                NavigationResult result = new NavigationResult(0);
                bool navigationSucceeded = true;
                if(navigationResults.Count > 0)
                {
                    result = navigationResults.Dequeue();
                    navigationSucceeded = result.Exception == null && result.NavigationHttpStatusCode >= 0;

                    if (!InitialNavigationFinished)
                    {
                        InitialNavigationFinished = true;                    
                    }
                }

                TaskCompletionSource<int> navLoadingTaskCompletionSource = navigationLoadingTasks.Dequeue();
                if(!navLoadingTaskCompletionSource.Task.IsCompleted)
                {
                    if(navigationSucceeded)
                    {
                        Logger.Instance.Write($"Set navigation status code to {result.NavigationHttpStatusCode}");
                        navLoadingTaskCompletionSource.SetResult(result.NavigationHttpStatusCode);
                    }
                    else
                    {
                        Logger.Instance.Write($"Set navigation Exception");
                        navLoadingTaskCompletionSource.SetException(result.Exception);
                    }
                }
            } else {                
                TaskCompletionSource<int> newNavLoadingTask = new TaskCompletionSource<int>();
                navigationLoadingTasks.Enqueue(newNavLoadingTask);
                navigationTasks.Enqueue(newNavLoadingTask.Task);
                Logger.Instance.Write($"Start Loading...");
            }          
        }

        protected override void OnLoadError(CefBrowser browser, CefFrame frame, CefErrorCode errorCode, string errorText, string failedUrl)
        {
            if (frame.IsMain)
            {
                Logger.Instance.Write($"Load error: '{errorCode.ToString()}', text = '{errorText}'");
                ApplicationException ex = new ApplicationException($"Error Code: {errorCode.ToString()}, Message: {errorText}");                
                navigationResults.Enqueue(new NavigationResult(ex));
            }
        }
    }
}