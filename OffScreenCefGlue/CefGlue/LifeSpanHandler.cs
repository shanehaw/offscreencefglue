using System.Collections.Generic;
using Xilium.CefGlue;

namespace OffScreenCefGlue.CefGlue
{
    internal class LifeSpanHandler : CefLifeSpanHandler
    {
        private readonly CefBrowserAccepter[] accepters;

        internal LifeSpanHandler(params CefBrowserAccepter[] accepters)
        {
            this.accepters = accepters;
        }

        protected override void OnAfterCreated(CefBrowser browser)
        {            
            Logger.Instance.Write($"Browser created with identifier = {browser.Identifier}");
            foreach(CefBrowserAccepter accepter in accepters)
            {
                accepter.Accept(browser);
            }            
        }

        protected override void OnBeforeClose(CefBrowser browser)
        {
            Logger.Instance.Write($"Browser closed with identifier = {browser.Identifier}");
        }

        protected override bool DoClose(CefBrowser browser)
        {
            Logger.Instance.Write($"Browser DoClose with identifier = {browser.Identifier}");
            return base.DoClose(browser);
        }
    }
}