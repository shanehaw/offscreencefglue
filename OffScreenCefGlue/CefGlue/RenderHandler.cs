using Xilium.CefGlue;
using System;
using System.IO;
using System.Runtime.InteropServices;
using SixLabors.ImageSharp.PixelFormats;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Formats.Png;

namespace OffScreenCefGlue.CefGlue
{
    internal class RenderHandler : CefRenderHandler
    {
        private readonly int windowHeight;
        private readonly int windowWidth;                
        private Image<Bgra32> image;
        public Image<Bgra32> Image
        {
            get
            {
                return image.Clone();
            }
        }        

        internal RenderHandler(int windowWidth, int windowHeight)
        {
            this.windowWidth = windowWidth;
            this.windowHeight = windowHeight;            
        }

        protected override bool GetRootScreenRect(CefBrowser browser, ref CefRectangle rect)
        {
            GetViewRect(browser, out rect);
            return true;
        }

        protected override bool GetScreenPoint(CefBrowser browser, int viewX, int viewY, ref int screenX, ref int screenY)
        {
            screenX = viewX;
            screenY = viewY;
            return true;
        }

        protected override void GetViewRect(CefBrowser browser, out CefRectangle rect)
        {
            rect = new CefRectangle(0, 0, windowWidth, windowHeight);            
        }

        protected override bool GetScreenInfo(CefBrowser browser, CefScreenInfo screenInfo)
        {
            return false;
        }

        protected override void OnPopupSize(CefBrowser browser, CefRectangle rect)
        {
        }

        protected override void OnPaint(CefBrowser browser, CefPaintElementType type, CefRectangle[] dirtyRects, IntPtr rawBuffer, int width, int height)
        {
            var stride = ((((width * 4) + 31) & ~31) >> 3);
            
            byte[] buffer = new byte[height * stride * 8];
            Marshal.Copy(rawBuffer, buffer, 0, buffer.Length);
            
            int counter = 0;                        
            Bgra32[] imageBytes = new Bgra32[height * width];
            for(int i = 0; i < buffer.Length - 4; i += 4) 
            {                
                imageBytes[counter] = new Bgra32(
                    buffer[i+2],
                    buffer[i+1],
                    buffer[i],
                    buffer[i+3]);

                
                counter++;
            }
            
            image = SixLabors.ImageSharp.Image.LoadPixelData(imageBytes, width, height);            
        }

        protected override void OnAcceleratedPaint(CefBrowser browser, CefPaintElementType type, CefRectangle[] dirtyRects, System.IntPtr sharedHandle)
        {

        }

        protected override void OnCursorChange(CefBrowser browser, IntPtr cursorHandle, CefCursorType type, CefCursorInfo customCursorInfo)
        {

        }

        protected override void OnScrollOffsetChanged(CefBrowser browser, double x, double y)
        {

        }

        protected override void OnImeCompositionRangeChanged(CefBrowser browser, CefRange selectedRange, CefRectangle[] characterBounds)
        {

        }

        protected override CefAccessibilityHandler GetAccessibilityHandler()
        {
            return null;
        }
    }
}