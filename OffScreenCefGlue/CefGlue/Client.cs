using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using OffScreenCefGlue.CefGlue.RenderProcess;
using OffScreenCefGlue.CefGlue.Tasks;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Formats.Png;
using SixLabors.ImageSharp.PixelFormats;
using Xilium.CefGlue;

namespace OffScreenCefGlue.CefGlue
{
    internal class Client : CefClient, Browser, CefBrowserAccepter
    {
        private Dictionary<string, int> windowsKeyMap = new Dictionary<string, int>()
        {
            { "0", 0x30 },
            { "1", 0x31 },
            { "2", 0x32 },
            { "3", 0x33 },
            { "4", 0x34 },
            { "5", 0x35 },
            { "6", 0x36 },
            { "7", 0x37 },
            { "8", 0x38 },
            { "9", 0x39 },
            { "a", 0x41 },
            { "b", 0x42 },
            { "c", 0x43 },
            { "d", 0x44 },
            { "e", 0x45 },
            { "f", 0x46 },
            { "g", 0x47 },
            { "h", 0x48 },
            { "i", 0x49 },
            { "j", 0x4A },
            { "k", 0x4B },
            { "l", 0x4C },
            { "m", 0x4D },
            { "n", 0x4E },
            { "o", 0x4F },
            { "p", 0x50 },
            { "q", 0x51 },
            { "r", 0x52 },
            { "s", 0x53 },
            { "t", 0x54 },
            { "u", 0x55 },
            { "v", 0x56 },
            { "w", 0x57 },
            { "x", 0x58 },
            { "y", 0x59 },
            { "z", 0x4A },
            { " ", 0x20 },
            { "ENTER", 0x0D },
            { "DELETE", 0x2E}
        };

        private readonly LoadHandler loadHandler;           
        private readonly RenderHandler renderHandler;
        private readonly LifeSpanHandler lifeSpanHandler;
        private readonly RequestHandler requestHandler;
        private readonly DisplayHandler displayHandler;
        private readonly DownloadHandler downloadHandler;
        private readonly CefBrowserSettings browserSettings;
        private readonly CefWindowInfo windowInfo;


        private TaskCompletionSource<string> jsExecuteTask;
        private TaskCompletionSource<object> jsInitialized;       
        private CefBrowser browser;        
        private Queue<Task<int>> navigationTasks;

        private Client(int windowWidth, int windowHeight, CefBrowserSettings settings, CefWindowInfo wndInfo)
        {                                    
            jsInitialized = new TaskCompletionSource<object>();
            (LoadHandler loadHandler, Queue<Task<int>> navigationTasks) = LoadHandler.Initialize();
            this.loadHandler = loadHandler;            
            this.navigationTasks = navigationTasks;
            renderHandler = new RenderHandler(windowWidth, windowHeight);
            lifeSpanHandler = new LifeSpanHandler(this, loadHandler);
            requestHandler = new RequestHandler();
            displayHandler = new DisplayHandler();
            downloadHandler = new DownloadHandler();
            this.browserSettings = settings;
            this.windowInfo = wndInfo;
        }

        public static ValueTuple<Client, NavigationTask> Initialize(int windowWidth, int windowHeight, CefBrowserSettings settings, CefWindowInfo wndInfo)
        {
            Client client = new Client(windowWidth, windowHeight, settings, wndInfo);
            NavigationTask initPageLoadTask = new NavigationTask(client.navigationTasks, client.jsInitialized.Task);
            return (client, initPageLoadTask);
        }

        protected override CefDownloadHandler GetDownloadHandler()
        {
            return downloadHandler;
        }        

        protected override CefRenderHandler GetRenderHandler()
        {            
            return renderHandler;
        }

        protected override CefLoadHandler GetLoadHandler()
        {
            return loadHandler;
        }

        protected override CefLifeSpanHandler GetLifeSpanHandler()
        {
            return lifeSpanHandler;
        }      

        protected override CefRequestHandler GetRequestHandler()
        {
            return requestHandler;
        }  

        protected override CefDisplayHandler GetDisplayHandler()
        {
            return displayHandler;
        }      

        protected override bool OnProcessMessageReceived(CefBrowser browser, CefFrame frame, CefProcessId sourceProcess, CefProcessMessage message)        
        {            
            if (message.Name == "jsReady") 
            {                
                if(!jsInitialized.Task.IsCompleted)
                {                    
                    jsInitialized.SetResult(true);
                    return true;
                }                
            }
            else if (sourceProcess == CefProcessId.Renderer)
            {
                if (message.Name == "jsExp")
                {
                    string exceptionMessage = message.Arguments.GetString(0);
                    if(jsExecuteTask.Task.IsCompleted && jsExecuteTask.Task.IsFaulted) {
                        Console.WriteLine($"Failed to set js exception: {jsExecuteTask.Task.Exception.ToString()}");                        
                    } else {
                        jsExecuteTask.SetException(new ApplicationException(exceptionMessage));
                    }
                    return true;
                }
                else if (message.Name == "jsResult")
                {
                    string result = message.Arguments.GetString(0);                    
                    jsExecuteTask.SetResult(result);
                    return true;
                }

            }
            return base.OnProcessMessageReceived(browser, frame, sourceProcess, message);
        }            

        private static Task<int> CombineTasks(Task<int> navTask, Task jsTask)
        {
            return new TaskFactory().ContinueWhenAll(new Task[] { navTask, jsTask }, ts => navTask.Result);
        }

        public void Quit()
        {
            CefRuntime.PostTask(CefThreadId.UI, new QuitTask());
        }

        public Task<string> ExecuteJS(string code)
        {
            jsExecuteTask = new TaskCompletionSource<string>();
            CefProcessMessage jsMessage = CefProcessMessage.Create("js");
            jsMessage.Arguments.SetString(0, code);
            browser.GetMainFrame().SendProcessMessage(CefProcessId.Renderer, jsMessage);
            return jsExecuteTask.Task;
        }

        public NavigationTask Navigate(string url)
        {             
            jsInitialized = new TaskCompletionSource<object>();
            Queue<Task<int>> navigationTasks = loadHandler.Navigate(url);            
            return new NavigationTask(navigationTasks, jsInitialized.Task);
        }

        public NavigationTask NavigateWithAuth(string url, string host, string userName, string password)
        {
            requestHandler.addAuthDetails(host, userName, password);
            return Navigate(url);
        }

        public Image<Bgra32> TakeScreenshot()
        {            
            return renderHandler.Image;
        }

        public void Accept(CefBrowser browser)
        {
            if(this.browser == null)
            {
                this.browser = browser;
                this.browser.GetHost().SendFocusEvent(true);
            }   
        }

        public void Click(int x, int y)
        {
            Logger.Instance.Write($"Click {x}:{y}");
            CefMouseEvent evt = new CefMouseEvent(x, y, CefEventFlags.LeftMouseButton);            
            this.browser.GetHost().SendMouseClickEvent(evt, CefMouseButtonType.Left, false, 1);            
            //this.browser.GetHost().SendMouseMoveEvent(evt, false);            
            this.browser.GetHost().SendMouseClickEvent(evt, CefMouseButtonType.Left, true, 1);
            //this.browser.GetHost().SendMouseMoveEvent(evt, true);
        }

        public void Type(string message)
        {
            Logger.Instance.Write($"Type: '{message}'");
            foreach(char c in message)
            {
                this.browser.GetHost().SendKeyEvent(CreateEventForType(c, CefKeyEventType.KeyDown));                
                this.browser.GetHost().SendKeyEvent(CreateEventForType(c, CefKeyEventType.RawKeyDown));                
                this.browser.GetHost().SendKeyEvent(CreateEventForType(c, CefKeyEventType.Char));                
                this.browser.GetHost().SendKeyEvent(CreateEventForType(c, CefKeyEventType.KeyUp));                
            }
        }        

        public void HitEnter()
        {
            Logger.Instance.Write("Start");
            this.browser.GetHost().SendKeyEvent(CreateEventForKey("ENTER", CefKeyEventType.KeyDown));  
            this.browser.GetHost().SendKeyEvent(CreateEventForKey("ENTER", CefKeyEventType.RawKeyDown));                        
            this.browser.GetHost().SendKeyEvent(CreateEventForKey("ENTER", CefKeyEventType.KeyUp));
            Logger.Instance.Write("End");
        }

        public void HitDelete()
        {
            Logger.Instance.Write("Start");
            this.browser.GetHost().SendKeyEvent(CreateEventForKey("DELETE", CefKeyEventType.KeyDown));  
            this.browser.GetHost().SendKeyEvent(CreateEventForKey("DELETE", CefKeyEventType.RawKeyDown));                        
            this.browser.GetHost().SendKeyEvent(CreateEventForKey("DELETE", CefKeyEventType.KeyUp));
            Logger.Instance.Write("End");
        }

        public void HitWithCtrl(string character)
        {
            Logger.Instance.Write($"Start - {character}");
            CefKeyEvent ev = null;
            ev = CreateEventForKey(character, CefKeyEventType.KeyDown);
            ev.Modifiers = CefEventFlags.ControlDown;
            this.browser.GetHost().SendKeyEvent(ev); 

            ev = CreateEventForKey(character, CefKeyEventType.RawKeyDown);
            ev.Modifiers = CefEventFlags.ControlDown;
            this.browser.GetHost().SendKeyEvent(ev); 

            ev = CreateEventForKey(character, CefKeyEventType.KeyUp);
            ev.Modifiers = CefEventFlags.ControlDown;
            this.browser.GetHost().SendKeyEvent(ev);
            Logger.Instance.Write("End");
        }

        private CefKeyEvent CreateEventForType(char c, CefKeyEventType type)
        {
            CefKeyEvent keyEvent = null;
            if(Char.IsUpper(c))
            {
                char lowerC = c.ToString().ToLower()[0];
                keyEvent = CreateEventForKey(lowerC.ToString(), type);
                keyEvent.Modifiers = CefEventFlags.ShiftDown;                                
            }
            else
            {
                keyEvent = CreateEventForKey(c.ToString(), type);
                keyEvent.Modifiers = CefEventFlags.None;                
            }
            keyEvent.Character = c;
            return keyEvent;
        }        

        private CefKeyEvent CreateEventForKey(string key, CefKeyEventType type)
        {
            CefKeyEvent keyEvent = new CefKeyEvent();            
            keyEvent.EventType = type;
            keyEvent.FocusOnEditableField = true;
            keyEvent.IsSystemKey = false;
            keyEvent.WindowsKeyCode = windowsKeyMap[key];            
            return keyEvent;
        }

        public void ShowDevTools()
        {
            this.browser.GetHost().ShowDevTools(windowInfo, this, browserSettings, new CefPoint(0,100));
        }
    }
}