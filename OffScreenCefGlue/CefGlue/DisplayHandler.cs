using System;
using System.Collections.Generic;
using Xilium.CefGlue;

namespace OffScreenCefGlue.CefGlue
{
    public class DisplayHandler: CefDisplayHandler
    {
        protected override bool OnConsoleMessage(CefBrowser browser, CefLogSeverity level, string message, string source, int line)
        {
            Logger.Instance.Write($@"On Console Message:
                level: {level}
                message: {message}
                source: {source}
                line: {line}
            ");
            return base.OnConsoleMessage(browser, level, message, source, line);
        }
    }
}