using Xilium.CefGlue;

namespace OffScreenCefGlue.CefGlue.Tasks
{
    public class QuitTask : CefTask
    {
        protected override void Execute()
        {
            CefRuntime.QuitMessageLoop();
        }
    }
}