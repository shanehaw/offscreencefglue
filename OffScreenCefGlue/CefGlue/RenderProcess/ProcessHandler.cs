using System;
using Xilium.CefGlue;

namespace OffScreenCefGlue.CefGlue.RenderProcess
{
    internal class ProcessHandler : CefRenderProcessHandler
    {
        protected override void OnContextCreated(CefBrowser browser, CefFrame frame, CefV8Context context)
        {
            Logger.Instance.Write("On Context Created.");            
            if (frame.IsMain)
            {                
                CefProcessMessage jsReady = CefProcessMessage.Create("jsReady");
                browser.GetMainFrame().SendProcessMessage(CefProcessId.Browser, jsReady);
            }
        }

        protected override void OnContextReleased(CefBrowser browser, CefFrame frame, CefV8Context context)
        {
            Logger.Instance.Write($"On Context Released.");
        }

        protected override bool OnProcessMessageReceived(CefBrowser browser, CefFrame frame, CefProcessId sourceProcess, CefProcessMessage message)
        {
            if (sourceProcess == CefProcessId.Browser && message.Name == "js")
            {
                CefV8Value ret = null;
                CefV8Exception exp = null;
                string js = message.Arguments.GetString(0);


                browser.GetMainFrame().V8Context.TryEval(js, js, 0, out ret, out exp);

                CefProcessMessage returnMessage = null;
                if (exp != null)
                {
                    Logger.Instance.Write($"Exception processing js - {exp.Message}");
                    returnMessage = CefProcessMessage.Create("jsExp");
                    returnMessage.Arguments.SetString(0, exp.Message);                    
                }
                else if (ret != null)
                {
                    Logger.Instance.Write($"Result of js - {ret.GetStringValue()}");
                    returnMessage = CefProcessMessage.Create("jsResult");
                    returnMessage.Arguments.SetString(0, ret.GetStringValue());
                }
                else
                {
                    Logger.Instance.Write($"Neither exception nor result from js - huh?");
                }

                if (returnMessage != null)
                {
                    browser.GetMainFrame().SendProcessMessage(CefProcessId.Browser, returnMessage);
                    return true;
                }
            }
            return false;
        }
    }
}