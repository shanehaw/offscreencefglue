using System;
using Xilium.CefGlue;
using OffScreenCefGlue.CefGlue.RenderProcess;

namespace OffScreenCefGlue.CefGlue
{
    internal class App : CefApp
    {
        protected override void OnBeforeCommandLineProcessing(string processType, CefCommandLine commandLine)
        {
            commandLine.AppendSwitch("disable-gpu", "1");
            commandLine.AppendSwitch("disable-gpu-compositing", "1");
            commandLine.AppendSwitch("disable-smooth-scrolling", "1");            
            commandLine.AppendSwitch("no-sandbox", "1");
            commandLine.AppendSwitch("no-zygote");
            commandLine.AppendSwitch("disable-gpu-sandbox", "1");
            commandLine.AppendSwitch("in-process-gpu", "1");
        }

        protected override CefRenderProcessHandler GetRenderProcessHandler()
        {
            return new ProcessHandler();
        }
    }
}