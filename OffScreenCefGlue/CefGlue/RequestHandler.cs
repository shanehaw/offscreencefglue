using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using OffScreenCefGlue.CefGlue.RenderProcess;
using OffScreenCefGlue.CefGlue.Tasks;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Formats.Png;
using SixLabors.ImageSharp.PixelFormats;
using Xilium.CefGlue;

namespace OffScreenCefGlue.CefGlue
{
    internal class RequestHandler : CefRequestHandler    
    {
        private Dictionary<String, ValueTuple<String, String>> authDetails;

        internal RequestHandler()
        {
            authDetails = new Dictionary<string, ValueTuple<string, string>>();
        }

        protected override CefResourceRequestHandler GetResourceRequestHandler(
            CefBrowser browser, 
            CefFrame frame, 
            CefRequest request, 
            bool isNavigation, 
            bool isDownload, 
            string requestInitiator, 
            ref bool disableDefaultHandling)
        {
            return null;
        }

        internal void addAuthDetails(string host, string user, string password)
        {
            authDetails[host] = (user, password);
        }

        protected override bool OnBeforeBrowse(CefBrowser browser, CefFrame frame, CefRequest request, bool userGesture, bool isRedirect)
        {
            if(frame.IsMain)
            {
                Logger.Instance.Write($"Request.Url: {request.Url}");    
            }
            return false;
        }

        protected override bool GetAuthCredentials(CefBrowser browser, string originUrl, bool isProxy, string host, int port, string realm, string scheme, CefAuthCallback callback)
        {
            Logger.Instance.Write($"originalUrl = {originUrl}, isProxy = {isProxy}, host = {host}, port = {port}, realm = {realm}, scheme = {scheme}");
            if(authDetails.ContainsKey(host))
            {
                (string userName, string password) = authDetails[host];                
                callback.Continue(userName, password);
                return true;
            }            
            return false;
        }
    }
}