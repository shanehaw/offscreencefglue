using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Formats.Png;
using SixLabors.ImageSharp.PixelFormats;

namespace OffScreenCefGlue
{
    public class TcpServer: BrowserGateway
    {        
        public TcpServer(Browser browser) : base(browser)
        {            
        }

        public override void Run()
        {            
            IPAddress ip = IPAddress.Any;
            TcpListener listener = new TcpListener(ip, 7777);
            listener.Start();
            bool tcpLoopFinished = false;
            try
            {
                NetworkStream clientStream = null;
                while (!tcpLoopFinished)
                {                    
                    TcpClient client = listener.AcceptTcpClient();
                    clientStream = client.GetStream(); 

                    byte[] messageBytes = new byte[1024];                    
                    string message = clientStream.ReadMessageFromStream();                    
                    messageBytes = null;
                    if (message == "kill")
                    {                        
                        clientStream.SendMessage(message);
                        browser.Quit();
                        tcpLoopFinished = true;
                    }
                    else if (message.StartsWith("navigate:"))
                    {
                        int offset = "navigate:".Length;
                        string navigationUrl = message.Substring(offset);
                        NavigationTask navigationTask = browser.Navigate(navigationUrl);

                        while(true)
                        {
                            Thread.Sleep(200);
                            if(navigationTask.IsCompleted())
                            {
                                Thread.Sleep(200);
                                if(navigationTask.IsCompleted())
                                {
                                    break;
                                }
                            }                                       
                        }                        

                        (Exception ex, List<int> httpStatusCodes) = navigationTask.NavigationResult();
                        clientStream.SendMessage($"navigate:{httpStatusCodes.Where(c => c > 0).Last()}");                        
                    }
                    else if(message.StartsWith("navigateWithAuth:"))
                    {
                        int offset = "navigateWithAuth:".Length;
                        string data = message.Substring(offset);
                        string[] dataParts = data.Split('|');
                        string url = dataParts[0];
                        string host = dataParts[1];
                        string userName = dataParts[2];
                        string password = dataParts[3];                        

                        NavigationTask navigationTask = browser.NavigateWithAuth(url, host, userName, password);
                        while(true)
                        {
                            Thread.Sleep(200);
                            if(navigationTask.IsCompleted())
                            {
                                Thread.Sleep(200);
                                if(navigationTask.IsCompleted())
                                {
                                    break;
                                }
                            }                                       
                        }   

                        (Exception ex, List<int> httpStatusCodes) = navigationTask.NavigationResult();                        
                        clientStream.SendMessage($"navigateWithAuth:{httpStatusCodes.Where(c => c > 0).Last()}");
                    }
                    else if (message.StartsWith("js:"))
                    {
                        int offset = "js:".Length;
                        string javaScript = message.Substring(offset).Trim();                        
                        Task<string> jsTask = browser.ExecuteJS(javaScript);
                        jsTask.WaitForCompletion();
                        clientStream.SendMessage(
                            jsTask.IsFaulted
                                ? $"jsException:{jsTask.Exception.ToString()}"
                                : $"jsResult:{jsTask.Result}");
                    }
                    else if (message == "screenshot")
                    {                        
                        Image<Bgra32> image = browser.TakeScreenshot();                                                                
                        image.Save(clientStream, new PngEncoder());
                    }
                    else if (message.StartsWith("click:"))
                    {
                        int offset = "click:".Length;
                        string coordinates = message.Substring(offset).Trim();
                        string[] coordGroups = coordinates.Split(':');
                        int x = int.Parse(coordGroups[0]);
                        int y = int.Parse(coordGroups[1]);
                        browser.Click(x, y);
                        clientStream.SendMessage("click");
                    }
                    else if(message.StartsWith("type:"))
                    {
                        int offset = "type:".Length;
                        string toBeTyped = message.Substring(offset).Trim();
                        browser.Type(toBeTyped);
                        clientStream.SendMessage("type");
                    }
                    else if(message == "hitEnter")
                    {
                        browser.HitEnter();
                        clientStream.SendMessage("hitEnter");
                    }
                    else if(message == "hitDelete")
                    {
                        browser.HitDelete();
                        clientStream.SendMessage("hitDelete");
                    }
                    else if(message.StartsWith("hitWithCtrl:"))
                    {
                        int offset = "hitWithCtrl:".Length;
                        string character = message.Last().ToString();
                        browser.HitWithCtrl(character);
                        clientStream.SendMessage("hitWithCtrl");
                    }
                    else if(message == "devTools")
                    {
                        browser.ShowDevTools();
                        clientStream.SendMessage("devTools");
                    }
                    else
                    {
                        clientStream.SendMessage($"Unrecognised command = '{message}'");                        
                    }
                    clientStream.Close();
                    clientStream.Dispose();
                }                
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }            
            listener.Stop();
        }        
    }
}