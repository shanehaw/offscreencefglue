﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using OffScreenCefGlue.CefGlue;
using OffScreenCefGlue.CefGlue.Tasks;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Formats.Png;
using SixLabors.ImageSharp.PixelFormats;
using Xilium.CefGlue;

namespace OffScreenCefGlue
{
    internal class Program
    {
        private static int Main(string[] args)
        {
            CefRuntime.Load();

            // Settings for all of CEF (e.g. process management and control).
            var settings = new CefSettings();
            settings.MultiThreadedMessageLoop = false;                     
            settings.LogSeverity = CefLogSeverity.Error;
            settings.LogFile = "cef.log";            
            settings.RemoteDebuggingPort = 20480;
            settings.NoSandbox = true;
            settings.WindowlessRenderingEnabled = true;   

            var argv = args;
            argv = new string[args.Length + 1];
            Array.Copy(args, 0, argv, 1, args.Length);
            argv[0] = "-";

            var cefMainArgs = new CefMainArgs(argv);
            var cefApp = new App();            

            if (CefRuntime.ExecuteProcess(cefMainArgs, cefApp, IntPtr.Zero) != -1)
            {
                Console.Error.WriteLine("CefRuntime could not the secondary process.");
                return -2;
            }

            // needed because CEF is multi process and will execute this code each time
            // with different arguments for each process
            foreach (var arg in args)
            {
                if (arg.StartsWith("--type="))
                {                    
                    return -2;
                }
            }            

            // Start the browser process (a child process).
            CefRuntime.Initialize(cefMainArgs, settings, cefApp, IntPtr.Zero);

            // Instruct CEF to not render to a window at all.
            CefWindowInfo cefWindowInfo = CefWindowInfo.Create();
            //cefWindowInfo.SetAsWindowless(IntPtr.Zero, true);

            // Settings for the browser window itself
            var cefBrowserSettings = new CefBrowserSettings();
            
            // The browser window will be 1920 x 1080 (pixels).
            (Client cefClient, NavigationTask initializeTask) = Client.Initialize(1920, 1080, cefBrowserSettings, cefWindowInfo);

            // Start up the browser instance.
            string url = "www.yahoo.com";
            CefBrowserHost.CreateBrowser(cefWindowInfo, cefClient, cefBrowserSettings, url);            
                        
            //Run Tcp thread
            Browser browser = cefClient;
            Thread t = new Thread(RunBrowserGatewayThread) { IsBackground = true };
            t.Start((browser, initializeTask));

            //Manual message loop
            Console.WriteLine("Begin running message loop");
            CefRuntime.RunMessageLoop();

            //Wait for TCP thread to finish before cleaning up
            Console.WriteLine("Waiting for tcp thread...");
            t.Join();

            // Clean up CEF.
            Console.WriteLine("Clean up CEF");
            CefRuntime.Shutdown();
            return -1;
        }

        private static void RunBrowserGatewayThread(object obj)
        {
            (Browser browser, NavigationTask initializationTask) = (ValueTuple<Browser, NavigationTask>) obj;
            WaitForInitialization(initializationTask);
            BrowserGateway gateway = new TcpServer(browser);
            gateway.Run();
        }

        private static void WaitForInitialization(NavigationTask initializationTask)
        {
            Console.Out.WriteLine("Wait for initialization");       
            while(true)
            {
                Thread.Sleep(200);
                if(initializationTask.IsCompleted())
                {
                    Thread.Sleep(200);
                    if(initializationTask.IsCompleted())
                    {
                        break;
                    }
                }                         
            }                    
            Console.WriteLine("Initialization finished"); 
        }
    }
}
