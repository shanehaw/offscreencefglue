using System.Threading.Tasks;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.PixelFormats;

namespace OffScreenCefGlue
{
    public interface Browser
    {
        void Quit();        

        NavigationTask Navigate(string url);

        Task<string> ExecuteJS(string js);

        NavigationTask NavigateWithAuth(string url, string host, string userName, string password);

        Image<Bgra32> TakeScreenshot();

        void Click(int x, int y);

        void Type(string message);

        void HitEnter();

        void HitDelete();

        void HitWithCtrl(string character);

        void ShowDevTools();
    }
}