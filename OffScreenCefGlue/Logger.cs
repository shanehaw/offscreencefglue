//\u001b[31m

using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.CompilerServices;

namespace OffScreenCefGlue
{
    public class Logger
    {
        private static readonly string ESC = "\u001b";
        private static readonly string NoColour = $"{ESC}[0m";        
        private static readonly string Black = $"{ESC}[30m";
        private static readonly string Red = $"{ESC}[31m";
        private static readonly string Green = $"{ESC}[32m";
        private static readonly string Yellow = $"{ESC}[33m";
        private static readonly string Blue = $"{ESC}[34m";
        private static readonly string Magenta = $"{ESC}[35m";
        private static readonly string Cyan = $"{ESC}[36m";
        private static readonly string White = $"{ESC}[37m";
        private static Logger instance;

        public static Logger Instance
        {
            get
            {
                return (instance == null)
                    ? (instance = new Logger())
                    : instance;
            }
        }

        private string[] colours = new string[]
        {
            Red,
            Green,
            Yellow,
            Blue,
            Magenta,
            Cyan,
            White
        };

        private Logger()
        {            
        }

        public void Write(string message, [CallerMemberName] string caller = "", [CallerFilePath] string callerFile = "")
        {
            string className = Path.GetFileNameWithoutExtension(callerFile);
            string classColour = GetClassColour(className);
            Console.WriteLine($"{classColour}[{className}.{caller}:{DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss.fff")}]{NoColour}: {message}");
        }

        private string GetClassColour(string className)
        {            
            return Blue;
        }
    }
}