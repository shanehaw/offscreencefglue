namespace OffScreenCefGlue
{
    public abstract class BrowserGateway
    {
        protected readonly Browser browser;
        public BrowserGateway(Browser browser)
        {
            this.browser = browser;
        }

        public abstract void Run();        
    }
}