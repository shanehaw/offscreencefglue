using System;
using Xunit;
using OffScreenCefGlue;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.PixelFormats;
using System.Threading;
using System.Net.Sockets;
using System.Text;
using SixLabors.ImageSharp.Formats.Png;

namespace OffScreenCefGlue.Tests
{
    public class TcpServerTests
    {
        private Thread StartServer(Browser browser)
        {
            TcpServer tcpServer = new TcpServer(browser);
            Thread tcpThread = new Thread(() => tcpServer.Run());
            tcpThread.Start();
            return tcpThread;
        }

        private void SendMessage(NetworkStream stream, string message)
        {
            byte[] messageBytes = Encoding.ASCII.GetBytes(message);
            stream.Write(messageBytes, 0, messageBytes.Length);
        }

        private TcpClient CreateTcpClient()
        {
            return new TcpClient("localhost", 7777);
        }

        private void SendMessage(string message)
        {
            SendMessageAndReceiveReply(message);
        }

        private String SendMessageAndReceiveReply(string message)
        {
            using(TcpClient client = CreateTcpClient())
            using(NetworkStream stream = client.GetStream())
            {
                SendMessage(stream, message);  
                return stream.ReadMessageFromStream();
            }
        }

        private Image<Bgra32> SendMessageAndReceiveImage(string message)
        {
            using(TcpClient client = CreateTcpClient())
            using(NetworkStream stream = client.GetStream())
            {
                SendMessage(stream, message);                  
                byte[] streamBytes = stream.ReadBytesFromStream();
                return Image.Load<Bgra32>(streamBytes);                
            }
        }

        [Fact]
        public void CanNavigate()
        {
            BrowserSpy browser = new BrowserSpy();
            Thread tcpThread = StartServer(browser);

            string command = "navigate";
            string address = "www.foo.bar";
            string message = $"{command}:{address}";
            string reply = SendMessageAndReceiveReply(message);
            SendMessage("kill");            
            tcpThread.Join();

            Assert.Equal(command, reply);
            Assert.Equal(address, browser.NavigationUrl);
        }

        [Fact]
        public void CanRunJs()
        {
            BrowserSpy browser = new BrowserSpy();
            Thread tcpThread = StartServer(browser);
            
            string command = "js";
            string js = @"(function() { const foo = 'Bar' }());";
            string message = $"{command}:{js}";
            string reply = SendMessageAndReceiveReply(message);
            SendMessage("kill");
            tcpThread.Join();

            Assert.Equal("jsResult:jsResult", reply);
            Assert.Equal(js, browser.JsExecuted);
        }

        [Fact]
        public void CanTakeScreenShot()
        {
            Bgra32[] imageBytes = new Bgra32[1];
            imageBytes[0] = new Bgra32(0,0,0);
            Image<Bgra32> image = Image.LoadPixelData(imageBytes, 1, 1);

            BrowserSpy browser = new BrowserSpy();
            browser.Screenshot = image;
            Thread tcpThread = StartServer(browser);

            Image<Bgra32> reply = SendMessageAndReceiveImage("screenshot");
            SendMessage("kill");
            tcpThread.Join();                        
            
            Assert.Equal(
                image.ToBase64String(PngFormat.Instance), 
                reply.ToBase64String(PngFormat.Instance));
        }

        [Fact]
        public void ReceiveNotificationWhenUnrecognizedCommand()
        {
            BrowserSpy browser = new BrowserSpy();
            Thread tcpThread = StartServer(browser);
            
            string command = "unrecognized command";     
            string reply = SendMessageAndReceiveReply(command);
            SendMessage("kill");
            tcpThread.Join();

            Assert.Equal($"Unrecognised command = '{command}'", reply);            
        }
    }
}
