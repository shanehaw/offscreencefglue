using System;
using OffScreenCefGlue;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.PixelFormats;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace OffScreenCefGlue.Tests
{
    public class BrowserSpy : Browser
    {            
        public string NavigationUrl { get; private set; }
        public string JsExecuted { get; private set; }
        public Image<Bgra32> Screenshot { get; set; }
        public Dictionary<string, Tuple<string, string>> AuthsProvided { get; private set; }
        public Queue<Tuple<int, int>> PointsClicked { get; private set; }
        public Queue<string> MessagesTyped { get; private set; }
        public int NumberOfTimesEnterWasHit { get; private set; }

        public BrowserSpy()
        {                
            NavigationUrl = null;
            JsExecuted = null;
        }

        public Task<string> ExecuteJS(string js)
        {
            JsExecuted = js;
            return Task.FromResult("jsResult");
        }

        public Task Navigate(string url)
        {
            NavigationUrl = url;
            return Task.CompletedTask;
        }

        public void Quit()
        {                
        }

        public Image<Bgra32> TakeScreenshot()
        {
            return Screenshot;
        }

        NavigationTask Browser.Navigate(string url)
        {
            Queue<Task<int>> navTasks = new Queue<Task<int>>();
            navTasks.Enqueue(Task.FromResult(200));
            NavigationTask navTask = new NavigationTask(navTasks, Task.CompletedTask);
            NavigationUrl = url;
            return navTask;
        }

        public NavigationTask NavigateWithAuth(string url, string host, string userName, string password)
        {
            Queue<Task<int>> navTasks = new Queue<Task<int>>();
            navTasks.Enqueue(Task.FromResult(200));
            NavigationTask navTask = new NavigationTask(navTasks, Task.CompletedTask);
            NavigationUrl = url;
            AuthsProvided[host] = Tuple.Create(userName, password);
            return navTask;
        }

        public void Click(int x, int y)
        {
            PointsClicked.Enqueue(Tuple.Create(x, y));
        }

        public void Type(string message)
        {
            MessagesTyped.Enqueue(message);
        }

        public void HitEnter()
        {
            NumberOfTimesEnterWasHit++;
        }

        public void HitDelete()
        {
            throw new NotImplementedException();
        }

        public void HitWithCtrl(string character)
        {
            throw new NotImplementedException();
        }
    }
}