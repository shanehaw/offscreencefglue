using ProtoBuf;

namespace OffScreenCefGlue.Common
{
    [ProtoContract]
    public class OffScreenCefGlueBrowserResponse
    {
        [ProtoMember(1)]
        public bool Success { get; set; }        

        [ProtoMember(2)]
        public string AdditionalData { get; set; }  
    }
}