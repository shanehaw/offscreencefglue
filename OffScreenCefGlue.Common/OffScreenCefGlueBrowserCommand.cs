﻿using ProtoBuf;

namespace OffScreenCefGlue.Common
{
    [ProtoContract]
    public class OffScreenCefGlueBrowserCommand
    {
        [ProtoMember(1)]
        public string CommandName { get; set; }

        [ProtoMember(2)]
        public string AdditionalData { get; set; }        
    }
}